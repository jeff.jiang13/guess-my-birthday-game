from random import randint
name = input("Hi! What is your name?")


for num_guesses in range(5):
    month = randint(1, 12)
    year = randint(1924, 2004)
    print("Guess ", num_guesses, ":", name, "were you born in", month, "/", year, "?")
    answer = input("yes or no? ")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif num_guesses == 4:
        print('I have other things to do. Good bye.')
    else:
        print("Drat! Lemme try again!")
